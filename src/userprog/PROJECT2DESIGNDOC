		     +--------------------------+
       	     |		CS 153				|
		     | PROJECT 2: USER PROGRAMS	|
		     | 	   DESIGN DOCUMENT     	|
		     +--------------------------+

---- GROUP ----

>> Fill in the names and email addresses of your group members.

Chwan-Hao Tung <ctung003@ucr.edu>
Tegvheer Sahni <tsahn001@ucr.edu>

---- PRELIMINARIES ----

>> If you have any preliminary comments on your submission, notes for the
>> TAs, or extra credit, please give them here.

>> Please cite any offline or online sources you consulted while
>> preparing your submission, other than the Pintos documentation, course
>> text, lecture notes, and course staff.

			   ARGUMENT PASSING
			   ================

---- DATA STRUCTURES ----

>> A1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.

struct 
exec_helper 
{
	/* Program to load. */
	const char *file_name;
	
	/*Add semaphore for loading (for resource race cases!)*/   
	struct semaphore proc_create;
    
    /* Add bool for determining if program loaded successfully */
    bool load_success;
    
    /* list of processes that are children to this process */
    struct list p_children;
    
    /* list_elem variable that will allow us to access things in the p_children list */
    struct list_elem children_elem;

};

The purpose of this struct is to hold important info about a process. It 
has the whole file name and can be passed to other functions.

---- ALGORITHMS ----

>> A2: Briefly describe how you implemented argument parsing.  How do
>> you arrange for the elements of argv[] to be in the right order?
>> How do you avoid overflowing the stack page?

We started off by first declaring a exec_helper struct. We initialized its
variables to the desired values. We then parsed the file name to get the 
actual file name which is located at the first spot. We pass this name to 
thread_create so that the thread can be created. In addition, we pass in 
the exec_helper struct as the last arguement to thread create. In the load function, we 
bring the executable and have the bin file point to it. Finally. in setup_stack
function, we parse the command line and push everything we need to push onto
the stack. 


To make sure that the elements in arg[v] are in the right order, we need to 
make sure that the pointers to the argumenets are in the correct order. It does not 
matter what order the actual elements are in since they will be accessed using
pointers. We will push the pointer that points to the first arguement first,
and so.

We will avoid overflowing the stack page by comparing the current stack 
position and the value of PHYS_BASE to see if we have gone over the border
and entered memory that we are not supposed to be in. 



---- RATIONALE ----

>> A3: Why does Pintos implement strtok_r() but not strtok()?
	
strtok() by itself does not store different string states if we're parsing
multiple strings at the same time. We can however pass different pointers to
strtok_r() to parsed different strings concurrently. This will allow multiple
threads to parse different strings concurrently without conflicts.

>> A4: In Pintos, the kernel separates commands into a executable name
>> and arguments.  In Unix-like systems, the shell does this
>> separation.  Identify at least two advantages of the Unix approach.

One advantage is that the parsing process would be easier to implement
in shell rather than the Kernel. This is because the shell has a lot
of powerful tools to do the job. The kernel shouldnt have to do this task,
because methods and tools to do this are already in the kernal and used 
by the shell.The kernal could use the time it saves to execute other tasks, 
thus leading to efficient use of the time.

Another advantage is that the shell would be able to check for faulty or 
potentially dangerous commandlines , in a more efficient manner. The shell 
can use more simpler and faster methods to parse the commandline that has 
error checking in it. These are the advantages in using a shell to 
seperate commands into executable name and arguements.


	

			     SYSTEM CALLS
			     ============

---- DATA STRUCTURES ----

>> B1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.
	In thread.h struct thread:
		struct list file_list;  /* track list of files this thread operates on */
		struct list_elem file_elem; 
		int return;				/* stores the return value of this thread */
		
		struct thread *parent;	/* used by child thread to check upon parent */
		struct semaphore *wait_sema /* used for process_wait */
		
		struct file_descriptor
		{
			int fd;					/* store the file descriptor of the file*/
			struct file *file;		/* should point to the corresponding file struct */
		}
		

>> B2: Describe how file descriptors are associated with open files.
>> Are file descriptors unique within the entire OS or just within a
>> single process?

We identify files by their file descriptors, we can set it to indicate that
the corresponding file has been opened or closed. It should be unique and be
used to interface between user space and kernel space.
File descriptors should be unique to a single process.
	
---- ALGORITHMS ----

>> B3: Describe your code for reading and writing user data from the
>> kernel.

In our code for reading and writing , we have a function called 
test_pointer to make sure that the pointer provided by the user will not 
cause problems. The cases that we should consider are if the user 
provides an invalid pointer, the pointer is not NULL, and the pointer 
does not go into kernel memory. We use the is_kernel_vaddr and the 
is_user_vaddr functions in vaddr.h, and the pagedir_get_page function in 
pagedir.c, to help us check that the pointer given is valid. After this
validation is complete, we can access data that needs to be read or 
written on. For example, we can access the contents of the thread stack
using the stack pointer. 

The test_pointer function will be called in our implementation of the 
read and write system calls. We make sure the pointer is correct, then
use the file system interface to access the file and do the appropriate 
action. This is how the reading and writing user data from the kernel 
works in our code.


>> B4: Suppose a system call causes a full page (4,096 bytes) of data
>> to be copied from user space into the kernel.  What is the least
>> and the greatest possible number of inspections of the page table
>> (e.g. calls to pagedir_get_page()) that might result?  What about
>> for a system call that only copies 2 bytes of data?  Is there room
>> for improvement in these numbers, and how much?

There at least needs to be one call to pagedir_get_page, and at most 
2 calls, because the data might be spread accross two pages at most. It is the same
for a system call that copies 2 bytes. We may be able to improve to only one call.


>> B5: Briefly describe your implementation of the "wait" system call
>> and how it interacts with process termination.
	
We call wait in syscall.c which in turn should call process_wait(). We would
need to get the thread that corresponds to the tid argument. Once we have that,
we need to check the status of the thread. If it has the status of THREAD_DYING,
then we can return -1 and finish. If the tid doesn't correspond to anything,
we return -1 and finish. If it is the child of some parent thread, then we 
need to check if the calling thread is our parent, if not, then we check
if the parent thread is still alive, if not, we return -1 and finish. Otherwise,
the calling thread needs to wait using sema_down and wait for the thread 
corresponding to the tid to raise the semaphore and exit. Then the calling 
thread must return the exit code returned by the tid thread.

	


>> B6: Any access to user program memory at a user-specified address
>> can fail due to a bad pointer value.  Such accesses must cause the
>> process to be terminated.  System calls are fraught with such
>> accesses, e.g. a "write" system call requires reading the system
>> call number from the user stack, then each of the call's three
>> arguments, then an arbitrary amount of user memory, and any of
>> these can fail at any point.  This poses a design and
>> error-handling problem: how do you best avoid obscuring the primary
>> function of code in a morass of error-handling?  Furthermore, when
>> an error is detected, how do you ensure that all temporarily
>> allocated resources (locks, buffers, etc.) are freed?  In a few
>> paragraphs, describe the strategy or strategies you adopted for
>> managing these issues.  Give an example.

In the course of our project, we will have to do a lot of error checking.
There are many different cases that need to be handled. 
Every pointer we need to operate on must be checked if it's null or not. 
In addition, when arguments are passed in, we must check their validity 
using is_user_vaddr given in vaddr.c. If its not then we must release all 
the locks we're using and free up all the buffers we've created or operated on.

We don't need to check whether if these resources are in use elsewhere, we just
free them anyways. This minimizes and simplifies error checking to a degree
so that it doesn't obscure what we're doing with a specific system call.

For example, for the "write" system call, we need to check if the arguments are valid.
We check if the file that corresponds to the file descriptor is in the list of files.
Then we need to check if the buffer is valid. We do this by the method described above to
check for validity, i.e does the buffer trespass the allocated user memory space.
Lastly, we check if the size argument is valid or not. i.e it goes out of bounds. 
If any of the condition above is determined to be invalid, we free up the file writing
lock, and all corresponding resources, such as the buffer.

Our overall strategy is to write the least code possible while still 
maintaining functionality. This will allow us to better debug the system 
and not obscure the code. 



---- SYNCHRONIZATION ----

>> B7: The "exec" system call returns -1 if loading the new executable
>> fails, so it cannot return before the new executable has completed
>> loading.  How does your code ensure this?  How is the load
>> success/failure status passed back to the thread that calls "exec"?

In the exec system call, we make a function call to start_process in process.c . 
This function makes a call to load. So, in the exec system call function, 
we will use a semaphore to synchronize the loading. The semaphore will 
be initialized to 0 so the thread that called exec will be forced to wait. 
The sema_up call to increment this sema will happen at the end of loading. 
The load function will have access to the semaphore because we will be 
passing in a exec_helper object. The success / failure status will be 
passed back to the thread that calls exec through the exec_helper object. 
We have created a bool variable for this in the exec_helper object.



>> B8: Consider parent process P with child process C.  How do you
>> ensure proper synchronization and avoid race conditions when P
>> calls wait(C) before C exits?  After C exits?  How do you ensure
>> that all resources are freed in each case?  How about when P
>> terminates without waiting, before C exits?  After C exits?  Are
>> there any special cases?

We can use a semaphore to ensure proper synchronization. The semaphore 
is initialized to 0. When a thread calls wait, it will get stuck in the 
semaphore loop because the semaphore’s value is 0. Once a child has 
finished processing, it will up the sema and exit. If P calls wait after 
process C exits, then the parent process must look through its list children, 
find the child, and look up the return status information in the 
exec_helper object. 

In each case, it is up to the parent to free up all the resources of 
the child. So, if a parent decides after looking up the status of a 
child it was waiting for ,and that some resources were not freed, then 
the parent must do it for the thread. It may call thread_exit for the 
child thread. If process P did not call wait(C), then it does not matter 
if P terminates before or after the child thread, because they are 2 
independent threads. 


---- RATIONALE ----

>> B9: Why did you choose to implement access to user memory from the
>> kernel in the way that you did?

When we receive pointers from the user, there's always a chance that the pointer
will cause trouble in the system. To handle this, our code implements a series
of checks to make the pointers are valid. Specifically, we use the is_kernel_vaddr 
and the is_user_vaddr functions in vaddr.h, and the pagedir_get_page function in 
pagedir.c, to help us check that the pointer given is valid. This makes sure
that anything given to the kernel cannot corrupt it. The way in which we implemented
our OS ensures safety. By using the filesystem interface, we are able to keep
our code simple.

>> B10: What advantages or disadvantages can you see to your design
>> for file descriptors?

One advantage to our design of file descriptors is that it is compact 
and very organized. All the information needed is packaged in a file 
descriptor struct object. This makes passing around information very 
easy.The concept of creating a file descriptor object for each opened 
file or I/O source makes it easy to keep track of the reading, writing, 
and organization of files. 

One possible disadvantage to our design of file descriptors is that if a
process opens a lot of files, then a lot of file descriptor structs will 
be created. This may possibly take up too much memory in the system. 


>> B11: The default tid_t to pid_t mapping is the identity mapping.
>> If you changed it, what advantages are there to your approach?

No, we didn't change that. For pintos at this stage, each process can only
correspond to the one thread. Thats why we are able to go from tid to pid, and vice
versa.
			   SURVEY QUESTIONS
			   ================

Answering these questions is optional, but it will help us improve the
course in future quarters.  Feel free to tell us anything you
want--these questions are just to spur your thoughts.  You may also
choose to respond anonymously in the course evaluations at the end of
the quarter.

>> In your opinion, was this assignment, or any one of the three problems
>> in it, too easy or too hard?  Did it take too long or too little time?

>> Did you find that working on a particular part of the assignment gave
>> you greater insight into some aspect of OS design?

>> Is there some particular fact or hint we should give students in
>> future quarters to help them solve the problems?  Conversely, did you
>> find any of our guidance to be misleading?

>> Do you have any suggestions for the TAs to more effectively assist
>> students, either for future quarters or the remaining projects?

>> Any other comments?
