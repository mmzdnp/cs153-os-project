#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

//Need these libraries
#include <stdbool.h>
#include <stdint.h>

void syscall_init (void);

/* We added these helper functions */
bool check_pointer (void* ptr_to_check);
bool check_buffer (void* buffer, int size_buff);
int* get_arguments ( int num_of_args, void *esp);


/* these are to use for the system calls. Each one helps implement a system call */
void halt_helper();
void exit_helper (int returnval);
uint32_t exec_syscall_helper (char* cmdline);
uint32_t wait_helper (int pid);
bool create_helper (char* filename, unsigned size_of_file);
bool remove_helper (char* filename);
uint32_t open_helper (char* filename);
uint32_t filesize_helper (int fd);
uint32_t read_helper (int fd, void* buff, unsigned size_buff);
uint32_t write_helper (int fd, void* buff, unsigned size_buff);
void seek_helper (int fd, unsigned position);
unsigned tell_helper (int fd);
uint32_t close_helper (int fd);

#endif /* userprog/syscall.h */
