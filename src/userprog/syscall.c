#include "userprog/syscall.h"
#include <stdio.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "pagedir.h"

/* we are declaring a global lock here. To use a 
   filesystem functionality, you need to get the lock. */
struct lock main_lock;

/* Copies a byte from user address USRC to kernel address DST.
   USRC must be below PHYS_BASE.
   Returns true if successful, false if a segfault occurred. */
static inline bool
get_user (uint8_t *dst, const uint8_t *usrc)
{
  int eax;
  asm ("movl $1f, %%eax; movb %2, %%al; movb %%al, %0; 1:"
       : "=m" (*dst), "=&a" (eax) : "m" (*usrc));
  return eax != 0;
}


/* Returns true if UADDR is a valid, mapped user address,
   false otherwise. */
static bool
verify_user (const void *uaddr) 
{
  return (uaddr < PHYS_BASE
          && pagedir_get_page (thread_current ()->pagedir, uaddr) != NULL);
}

/* Copies SIZE bytes from user address USRC to kernel address
   DST.
   Call thread_exit() if any of the user accesses are invalid. */
static void
copy_in (void *dst_, const void *usrc_, size_t size) 
{
  uint8_t *dst = dst_;
  const uint8_t *usrc = usrc_;
 
  for (; size > 0; size--, dst++, usrc++) 
    if (usrc >= (uint8_t *) PHYS_BASE || !get_user (dst, usrc) || !verify_user(usrc)) 
      thread_exit ();
}

static void syscall_handler (struct intr_frame *);

void
syscall_init (void) 
{
  intr_register_int (0x30, 3, INTR_ON, syscall_handler, "syscall");
  lock_init(&main_lock);
}

/* halt os helper */
void 
halt_helper()
{
	shutdown();
}

/* exit process helper */
void 
exit_helper (int returnval)
{
	thread_current()->return_status = returnval;
	printf("%s: exit(%d)\n", thread_name(),thread_current()->return_status);
	thread_exit();
}

/* start new process helper */
uint32_t 
exec_syscall_helper (char* cmdline)
{
	return process_execute(cmdline);
}

/* wait for child process to die helper */
uint32_t 
wait_helper (int pid)
{
	return process_wait(pid);
}

/* create a file helper */
bool		
create_helper (char* filename, unsigned size_of_file)
{
	bool result = false;
	
	/* this checks if the filename passed in is not NULL */
	if (filename == NULL)
	{
		thread_current()->return_status = -1;
		printf("%s: exit(%d)\n", thread_name(), -1);
		thread_exit();
	}
		
	/* getting the lock to do filesystem create call */
	lock_acquire(&main_lock);
	
	/* accessing the shared resource */
	result = filesys_create(filename, size_of_file); 
	
	/* releasing the lock because done with critical section */
	lock_release(&main_lock);
	
	return result;
}

/* remove a file helper */
bool
remove_helper (char* filename)
{
	return filesys_remove(filename);
}

/* open a file helper */
uint32_t 
open_helper (char* filename)
{
	/* this checks if the filename passed in is not NULL */
	if (filename == NULL)
	{
		thread_current()->return_status = -1;
		printf("%s: exit(%d)\n", thread_name(),-1);
		thread_exit();
	}
	
	/* getting the lock to do filesystem open call */
	lock_acquire(&main_lock); 
	
	struct file *new_file_info = filesys_open(filename);
	
	lock_release(&main_lock);
	
	//If no such file exist (NULL) return -1
	if(!new_file_info)
		return -1;
	
	/* creating the file descriptor */
	struct file_descriptor *new_fd = (struct file_descriptor *)malloc(sizeof(struct file_descriptor));
	
	/* initializing the contents of fd struct */
	new_fd->fd = thread_current()->next_fd_to_assign;
	++(thread_current()->next_fd_to_assign);
	new_fd->file_info = new_file_info;
	
	/* adding fd to list of all fds */
	lock_acquire(&main_lock); 
	list_push_front(&thread_current()->file_list , &new_fd->file_elem);
	lock_release(&main_lock); 
	return new_fd->fd;
}

/* get filesize helper */
uint32_t 
filesize_helper (int fd)
{
	int returnval = -1;
	struct list_elem *i;
	struct file_descriptor *fileD;
	lock_acquire(&main_lock); 
	struct list *file_list = &thread_current()->file_list;
	
	for(i= list_begin(file_list); i!=list_end(file_list); i = list_next (i))
	{
		fileD = list_entry(i, struct file_descriptor, file_elem);
		if (fileD->fd == fd)
		{
			
			returnval =  file_length(fileD->file_info);
			
		}
	}
	lock_release(&main_lock); 
	return returnval;
}

/* read file helper */
uint32_t 
read_helper (int fd, void* buff, unsigned size_buff)
{
	int count=0;
	
	/* if we're reading from user input */
	if (fd == 0)
	{
		while (count < size_buff)
		{
			char input = input_getc ();
			
			/*if we get a newline, we null terminate input */
			if (input == '\n')
			{
				*(char*)(buff+count)='\0';
				break;
			}
			*(char*)(buff+count)=input;
			count++;
		}
		return count;
	}
	
	/* this case is for a normal file, find the fd */
	lock_acquire(&main_lock);
	struct list_elem *i;
	struct file_descriptor *fileD;
	struct list* file_list = &thread_current()->file_list;
	
	for (i = list_begin (file_list); i!= list_end (file_list);
			i = list_next (i))
	{
		fileD = list_entry(i, struct file_descriptor, file_elem);
		if (fileD->fd == fd)
		{
			count=file_read(fileD->file_info, buff, size_buff);
		}
	}
	
	lock_release(&main_lock);
	
	return count;
}

/* write helper */
uint32_t 
write_helper (int fd, void* buff, unsigned size_buff)
{
	/* this is if we are writing to standard output */
	if (fd == STDOUT_FILENO)
	{
		putbuf(buff,size_buff);
		return size_buff;
	}
	lock_acquire(&main_lock);
	
	/* if we're writing to a regular file*/
	struct list_elem *i;
	struct file_descriptor *fileD;
	int count = 0;
	struct list* file_list = &thread_current()->file_list;
	
	for (i = list_begin (file_list); i!= list_end (file_list);
			i = list_next (i))
	{
		fileD = list_entry(i, struct file_descriptor, file_elem);
		if (fileD->fd == fd)
		{
			count=file_write(fileD->file_info, buff, size_buff);
		}
	}
	
	lock_release(&main_lock);
	
	return count;
}

/* seek helper */
void 
seek_helper (int fd, unsigned position)
{
	lock_acquire(&main_lock);
	
	/* iterate through file_list, find the correct fd, file_seek(file,args[1]), no return*/
	struct list_elem *i;
	struct file_descriptor *fileD;
	struct list* file_list = &thread_current()->file_list;
	
	for(i = list_begin(file_list); i != list_end(file_list); i = list_next(i))
	{
		fileD = list_entry(i, struct file_descriptor, file_elem);
		if (fileD->fd == fd)
		{
			file_seek(fileD->file_info,position);
		}
	}
	lock_release(&main_lock);	
}

/* tell helper */
unsigned  
tell_helper (int fd)
{
	/* iterate through file_list, find the correct fd, return file_tell(file)*/
	lock_acquire(&main_lock);
	
	struct list_elem *i;
	struct file_descriptor *fileD;
	struct list* file_list = &thread_current()->file_list;
	
	for(i = list_begin(file_list); i != list_end(file_list); i = list_next(i))
	{
		fileD = list_entry(i, struct file_descriptor, file_elem);
		if (fileD->fd == fd)
		{
			return file_tell(fileD->file_info);
		}
	}
	lock_release(&main_lock);
}

/* close helper */
uint32_t  
close_helper (int fd)
{
	/* iterate through file_list, find the correct fd, return file_close(file)*/
	/* then we need to remove that fd from file_list*/
	lock_acquire(&main_lock); 
	
	struct list_elem *i;
	struct file_descriptor *fileD;
	struct list* file_list = &thread_current()->file_list;
	
	for(i = list_begin(file_list); i != list_end(file_list); i = list_next(i))
	{
		fileD = list_entry(i, struct file_descriptor, file_elem);
		if (fileD->fd == fd)
		{
			list_remove(&fileD->file_elem);
			lock_release(&main_lock);
			return file_close(fileD->file_info);

			free(fileD); 
		}
	}
}


static void
syscall_handler (struct intr_frame *f UNUSED) 
{
	/*make sure we're not going to a bad address, 0x08048000 is the 
	  bottom of the virtual address */	
	
	if (!is_user_vaddr(f->esp) || f->esp < (void*)0x08048000)
	{
		thread_current()->return_status = -1;
		printf ("%s: exit(%d)\n", thread_name(),-1);
		thread_exit();
	}
	
	/* we need to get the syscall number.*/	
	int args[3];   
	    
	
	
	/* now we are checking all the possible call numbers and calling
	   the helper function */   
	if ((*(int*)(f->esp)) == SYS_HALT)
		halt_helper();
		
	else if ((*(int*)(f->esp)) == SYS_EXIT)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 1);
		exit_helper(args[0]);
	}
	else if ((*(int*)(f->esp)) == SYS_EXEC)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 1);
		f->eax = exec_syscall_helper((char*)args[0]);
	}
	
	else if ((*(int*)(f->esp)) == SYS_WAIT)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 1);	
		f->eax = wait_helper(args[0]);
	}
		
	else if ((*(int*)(f->esp)) == SYS_CREATE)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 2);	
		f->eax = create_helper((char*)args[0],(unsigned)args[1]);
	}
		
	else if ((*(int*)(f->esp)) == SYS_REMOVE)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 1);	
		f->eax = remove_helper((char*)args[0]);
	}
		
	else if ((*(int*)(f->esp)) == SYS_OPEN)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 1);	
		f->eax = open_helper((char*)args[0]);
	}
		
	else if ((*(int*)(f->esp)) == SYS_FILESIZE)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 1);	
		f->eax = filesize_helper(args[0]);
	}
		
	else if ((*(int*)(f->esp)) == SYS_READ)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 3);	
		f->eax = read_helper(args[0],(void*)args[1],(unsigned)args[2]);
	}
		
	else if ((*(int*)(f->esp)) == SYS_WRITE)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 3);	
		f->eax = write_helper(args[0],(void*)args[1],(unsigned)args[2]);
	}
		
	else if ((*(int*)(f->esp)) == SYS_SEEK)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 2);	
		seek_helper(args[0],(unsigned)args[1]);
	}
		
	else if ((*(int*)(f->esp)) == SYS_TELL)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 1);	
		f->eax = tell_helper(args[0]);
	}
	
	else if ((*(int*)(f->esp)) == SYS_CLOSE)
	{
		copy_in (args, (uint32_t *) f->esp + 1, sizeof(*args) * 1);	
		close_helper(args[0]);
	}
		
	else 
	{
		/* we have gotten an undefined system call if we are here */
		printf("Undefined system call! (%d) \n", (f->esp));
		thread_exit();
	}
}
